(function() {
  'use strict';

  angular
    .module('starter')
    .service('FeedbackService', FeedbackService);

  ///** @ngInject */
  FeedbackService.$inject = ['$firebaseArray', '$q'];
  function FeedbackService($firebaseArray, $q) {
    var service = {
      submitFeedback: submitFeedback,
      getFeedback: getFeedback,
      getQuestionsFromFireBase: getQuestionsFromFirebase
    };

    return service;

    /** @ngInject */
    function submitFeedback(answers) {

      var defer = $q.defer();
      var answersFirebase = new Firebase("https://sgti-btcamp-feedback.firebaseio.com/Answers");
      $firebaseArray(answersFirebase).$add(answers).then(function() {
        console.log('feedback submitted successfully');
        defer.resolve();
      }, function() {
        defer.reject();
      });

      return defer.promise;
    }

    function getFeedback(rows) {
      var feedbackFirebase = new Firebase("https://sgti-btcamp-feedback.firebaseio.com/Answers");
      var query = feedbackFirebase.orderByChild("timestamp").limitToLast(rows);
      return $firebaseArray(query);
    }

    function getQuestionsFromFirebase() {
      var questionnaire = new Firebase("https://sgti-btcamp-feedback.firebaseio.com/Questionnaire");
      return $firebaseArray(questionnaire);
    }

  }

})();
