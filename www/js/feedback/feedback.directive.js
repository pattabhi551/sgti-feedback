(function() {
  'use strict';

  angular
    .module('starter')
    .directive('feedback', feedback);

  /** @ngInject */
  function feedback() {
    var directive = {
      restrict: 'E',
      templateUrl: 'templates/feedback.html',
      scope: {},
      controller: 'FeedbackController',
      controllerAs: 'feedbackCtrl',
      bindToController: true
    };

    return directive;
  }

})();
