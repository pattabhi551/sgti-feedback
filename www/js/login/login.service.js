(function() {
  'use strict';

  angular
    .module('starter')
    .service('LoginService', LoginService);

  ///** @ngInject */
  LoginService.$inject = ['$q'];
  function LoginService($q) {


    var authData;

    var service = {
      getAuthData: getAuthData,
      setAuthData: setAuthData
    };

    return service;


    function getAuthData() {
      return authData;
    }

    function setAuthData(auth) {
      authData = auth;
    }

  }

})();

