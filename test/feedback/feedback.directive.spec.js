'use strict';

describe('Feedback Directive', function(){
    var $compile, scope;

    var element;

    beforeEach(module('starter'));

    beforeEach(inject(function(_$compile_, _$rootScope_){
      $compile = _$compile_;
      scope = _$rootScope_.$new();

      element = $compile("<feedback></feedback>")(scope);
      scope.$digest();

    }));

    it('should be defined', function() {
      expect(element).toBeDefined();
    });

    it('should compile the necessary elements', function(){
      expect(element.find('p')).toBeDefined();
      expect(element.find('p').text()).toEqual('testing stuff in feedback tab');
    });

    it('should contain a question element', function(){
    });

    it('should contain choices for answering', function() {
    });

    it('should')


});
