(function() {
  'use strict';

  angular
    .module('starter')
    .controller('LoginController', LoginController);

  /** @ngInject */
  //LoginController.$inject = ['$state', 'LoginService'];
  function LoginController($state, LoginService) {
    console.log('initiating this');
    var vm = this;

    vm.authData;

    var usersRef = new Firebase("https//sgti-btcamp-feedback.firebaseio.com");

    vm.login = function() {

      usersRef.authWithOAuthRedirect("facebook", function(error,authData) {
        if (error.code === "TRANSPORT_UNAVAILABLE") {
          usersRef.authWithOAuthPopup("facebook", function(error, authData) {
            if (error) {
              console.log("Login Failed!", error);
            } else {
              console.log("Authenticated successfully with payload:", authData);
              vm.authData = authData;
              $state.reload();
            }
          },{
            remember: "sessionOnly"
          });
        } else {
          console.log('logged in buddy');
          vm.authData = authData;
        }
      //  $state.reload();
      });
    };

    vm.logout = function() {
      usersRef.unauth(function (){
        console.log('user logged out');
      });
    };

    usersRef.onAuth(function(authData) {
      if (authData === null) {
        console.log("Not logged in yet");
      } else {
        console.log("Logged in as", authData.uid);
      }

      LoginService.setAuthData(authData);
      vm.authData = authData; // This will display the user's name in our view
    });
  }

})();
