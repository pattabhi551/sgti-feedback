(function() {
  'use strict';

  angular
    .module('starter')
    .controller('ViewController', ViewController);

  /** @ngInject */
  function ViewController($state, FeedbackService) {
    var vm = this;

    vm.feedback;

    vm.feedback = FeedbackService.getFeedback(100);

    vm.fbName = function(username) {
      return username.replace('facebook:', '');
    }
  }

})();
