(function() {
  'use strict';

  angular
    .module('starter')
    .controller('FeedbackController', FeedbackController);

  /** @ngInject */
  function FeedbackController(FeedbackService, LoginService, $state) {
    console.log('initiating this');
    var vm = this;

    var questions = FeedbackService.getQuestionsFromFireBase();

    vm.answers =[];
    vm.feedbackSubmitted = false;

    questions.$loaded().then(function() {
      vm.feedbackQuestions = questions;
    });

    vm.submitAnswers = function() {
        var authData = LoginService.getAuthData();
        var date = new Date();
        var userName = 'anonymous';
        var userId = 'anonymous';

        console.log('authData', authData);

        if(authData !== undefined) {
          userName = authData.facebook.displayName;
          userId = authData.uid;
        }
        var feedbackInfo = {
          user: userName,
          userId: userId,
          timestamp: date.getTime(),
          answers: vm.answers
        };
        FeedbackService.submitFeedback(feedbackInfo).then(function(){
          vm.feedbackSubmitted = true;
        });
    };
  }

})();
